#include "crypto_wrap.h"

RSA_KEYS::RSA_KEYS(){}

int RSA_KEYS::generate_keys(const int& length)
{
	try
	{
		params.GenerateRandomWithKeySize(rng, length);
		publicKey = CryptoPP::RSA::PublicKey(params);
		privateKey = CryptoPP::RSA::PrivateKey(params);
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Generate keys error: %s\n", e.what());
		return -1;
	}
}

int Base64Encode(const std::string& source, std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::StringSource(source, true,
			new CryptoPP::Base64Encoder(
				new CryptoPP::StringSink(out), false
			) // Base64Encoder
		);
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Base64 encode error: %s\n", e.what());
		return -1;
	}
}

int Base64Decode(const std::string& source, std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::StringSource(source, true,
			new CryptoPP::Base64Decoder(
				new CryptoPP::StringSink(out)
			) // Base64Decoder
		);
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Base64 decode error: %s\n", e.what());
		return -1;
	}
}

int AES_KEYS::encrypt(const std::string& source, std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::CFB_Mode<CryptoPP::AES>::Encryption encryptor(key, key.size(), iv);
		CryptoPP::StringSource(source, true,
			new CryptoPP::StreamTransformationFilter(encryptor,
				new CryptoPP::StringSink(out)
			) // StreamTransformationFilter      
		);
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Encrypt error: %s\n", e.what());
		return -1;
	}
}

int AES_KEYS::decrypt(const std::string& source, std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::CFB_Mode<CryptoPP::AES>::Decryption decryptor(key, key.size(), iv);
		CryptoPP::StringSource(source, true,
			new CryptoPP::StreamTransformationFilter(decryptor,
				new CryptoPP::StringSink(out)
			) // StreamTransformationFilter
		);
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Decrypt error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::encrypt(const std::string& source, std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::RSAES_OAEP_SHA_Encryptor e = CryptoPP::RSAES_OAEP_SHA_Encryptor(publicKey);
		CryptoPP::StringSource(source, true, new CryptoPP::PK_EncryptorFilter(rng, e, new CryptoPP::StringSink(out)));
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Encrypt error: %s\n", e.what());
		return -1;
	}
}


int RSA_KEYS::decrypt(const std::string& source, std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::RSAES_OAEP_SHA_Decryptor d = CryptoPP::RSAES_OAEP_SHA_Decryptor(privateKey);
		CryptoPP::StringSource(source, true, new CryptoPP::PK_DecryptorFilter(rng, d, new CryptoPP::StringSink(out)));
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Decrypt error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::sign(const std::string& source, std::string& signature)
{
	try
	{
		CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA256>::Signer signer(privateKey);
		signature.clear();

		CryptoPP::StringSource(source, true,
			new CryptoPP::SignerFilter(rng, signer,
				new CryptoPP::StringSink(signature)
			) // SignerFilter
		);
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Sign error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::verify(const std::string& source, const std::string& signature)
{
	try
	{
		CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA256>::Verifier verifier(publicKey);

		CryptoPP::StringSource(source + signature, true,
			new CryptoPP::SignatureVerificationFilter(
				verifier, NULL,
				CryptoPP::SignatureVerificationFilter::THROW_EXCEPTION
			) // SignatureVerificationFilter
		); // StringSource
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Verify error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::load_public_key(const std::string& filename)
{
	try
	{
		CryptoPP::ByteQueue queue;
		CryptoPP::FileSource file(filename.c_str(), true /*pumpAll*/);

		file.TransferTo(queue);
		queue.MessageEnd();

		CryptoPP::RSA::PublicKey pK;
		pK.Load(queue);
		if (!pK.Validate(rng, 3)) return -2;
		publicKey = pK;
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Load public key error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::save_public_key(const std::string& filename)
{
	try
	{
		CryptoPP::ByteQueue queue;
		CryptoPP::FileSink file(filename.c_str());

		publicKey.Save(queue);
		queue.CopyTo(file);
		file.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Save public key error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::load_private_key(const std::string& filename)
{
	try
	{
		CryptoPP::ByteQueue queue;
		CryptoPP::FileSource file(filename.c_str(), true /*pumpAll*/);

		file.TransferTo(queue);
		queue.MessageEnd();

		CryptoPP::RSA::PrivateKey pK;
		pK.Load(queue);
		if (!pK.Validate(rng, 3)) return -2;
		privateKey = pK;
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Load private key error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::save_private_key(const std::string& filename)
{
	try
	{
		CryptoPP::ByteQueue queue;
		CryptoPP::FileSink file(filename.c_str());
		privateKey.Save(queue);

		publicKey.Save(queue);
		queue.CopyTo(file);
		file.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Save private key error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::base64encode_private_key(std::string& out) 
{
	try
	{
		out.clear();
		CryptoPP::Base64Encoder encoder(new CryptoPP::StringSink(out), false);
		privateKey.DEREncode(encoder);
		encoder.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Private key base64 encode error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::base64decode_private_key(std::string& key) 
{ 
	try
	{
		CryptoPP::StringSource ss(key, true, new CryptoPP::Base64Decoder);
		CryptoPP::RSA::PrivateKey pK;
		pK.BERDecode(ss);
		if (!pK.Validate(rng, 3)) return -2;
		privateKey = pK;
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Private key base64 decode error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::base64encode_public_key(std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::Base64Encoder encoder(new CryptoPP::StringSink(out), false);
		publicKey.DEREncode(encoder);
		encoder.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Public key base64 encode error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::base64decode_public_key(std::string& key)
{ 
	try
	{
		CryptoPP::StringSource ss(key, true, new CryptoPP::Base64Decoder);
		CryptoPP::RSA::PublicKey pK;
		pK.BERDecode(ss);
		if (!pK.Validate(rng, 3)) return -2;
		publicKey = pK;
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Public key base64 decode error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::get_private_key(std::string& out) 
{
	try
	{
		out.clear();
		CryptoPP::StringSink s(out);
		privateKey.DEREncode(s);
		s.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Writing private key to std::string error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::set_private_key(std::string& key)
{
	try
	{
		CryptoPP::StringSource ss(key, true);
		CryptoPP::RSA::PrivateKey pK;
		pK.BERDecode(ss);
		if (!pK.Validate(rng, 3)) return -2;
		privateKey = pK;
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Loading private key from std::string error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::get_public_key(std::string& out) 
{
	try
	{
		out.clear();
		CryptoPP::StringSink s(out);
		publicKey.DEREncode(s);
		s.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Writing public key to std::string error: %s\n", e.what());
		return -1;
	}
}

int RSA_KEYS::set_public_key(std::string& key) 
{
	try
	{
		CryptoPP::StringSource ss(key, true);
		CryptoPP::RSA::PublicKey pK;
		pK.BERDecode(ss);
		if (!pK.Validate(rng, 3)) return -2;
		publicKey = pK;
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[RSA] Loading public key from std::string error: %s\n", e.what());
		return -1;
	}
}

AES_KEYS::AES_KEYS(){}

int AES_KEYS::generate_keys(const int& length)
{
	try
	{
		CryptoPP::AutoSeededRandomPool rnd;

		iv.CleanGrow(length);
		rnd.GenerateBlock(iv, length);

		key.CleanGrow(length);
		rnd.GenerateBlock(key, length);

		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Generate keys error: %s\n", e.what());
		return -1;
	}
}

int AES_KEYS::get_key(std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::StringSink s(out);
		s.Put(key.data(), key.size());
		s.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Writing key to std::string error: %s\n", e.what());
		return -1;
	}
}

int AES_KEYS::set_key(std::string& source)
{
	try
	{
		key.CleanGrow(source.size());
		CryptoPP::StringSource(source, true,
			new CryptoPP::ArraySink(key.data(), source.size()));
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Loading key from std::string error: %s\n", e.what());
		return -1;
	}
}

int AES_KEYS::get_iv(std::string& out)
{
	try
	{
		out.clear();
		CryptoPP::StringSink s(out);
		s.Put(iv.data(), iv.size());
		s.MessageEnd();
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Writing IV to std::string error: %s\n", e.what());
		return -1;
	}
}

int AES_KEYS::set_iv(std::string& source)
{
	try
	{
		iv.CleanGrow(source.size());
		CryptoPP::StringSource(source, true,
			new CryptoPP::ArraySink(iv.data(), source.size()));
		return 0;
	}
	catch (std::exception& e)
	{
		printf("[AES] Loading IV from std::string error: %s\n", e.what());
		return -1;
	}
}
