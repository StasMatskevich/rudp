#ifndef RSA_ENCRYPT_H
#define RSA_ENCRYPT_H

#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#include <string>
#include <modes.h>
#include <rsa.h>
#include <aes.h>
#include <base64.h>
#include <pssr.h>
#include <sha.h>
#include <filters.h>
#include <files.h>
#include <osrng.h>
#include <secblock.h>
#include <cryptlib.h>
#include <queue.h>
#include <files.h>

class RSA_KEYS
{
	private:
	CryptoPP::AutoSeededRandomPool rng;
	CryptoPP::RSA::PrivateKey privateKey;
	CryptoPP::RSA::PublicKey publicKey;
	CryptoPP::InvertibleRSAFunction params;

	public:
	RSA_KEYS();

	int generate_keys(const int& length);
	int load_public_key(const std::string& filename);
	int save_public_key(const std::string& filename);
	int load_private_key(const std::string& filename);
	int save_private_key(const std::string& filename);
	int base64encode_private_key(std::string& out);
	int base64decode_private_key(std::string& key);
	int base64encode_public_key(std::string& out);
	int base64decode_public_key(std::string& key);
	int get_private_key(std::string& out);
	int set_private_key(std::string& key);
	int get_public_key(std::string& out);
	int set_public_key(std::string& key);

	int encrypt(const std::string& source, std::string& out);
	int decrypt(const std::string& source, std::string& out);

	int sign(const std::string& source, std::string& signature);
	int verify(const std::string& source, const std::string& signature);
};

class AES_KEYS
{
	private:
	CryptoPP::SecByteBlock key;
	CryptoPP::SecByteBlock iv;

	public:
	AES_KEYS();

	int generate_keys(const int& length);
	int get_key(std::string& out);
	int set_key(std::string& source);
	int get_iv(std::string& out);
	int set_iv(std::string& source);

	int encrypt(const std::string& source, std::string& out);
	int decrypt(const std::string& source, std::string& out);
};

int Base64Encode(const std::string& source, std::string& out);
int Base64Decode(const std::string& source, std::string& out);

#endif
