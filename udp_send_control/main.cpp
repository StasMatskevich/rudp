#define _CRT_SECURE_NO_WARNINGS

#include "udp_controller.h"
#include "crypto_wrap.h"

#include <iostream>
#include <fstream>

void controller_listener(std::string& data, unsigned int address, unsigned short port)
{
	printf("Got a message. Size is %d\n", data.size());
}

void read_file(const std::string& path, std::string& file)
{
	std::ifstream fin(path, std::ifstream::in | std::ifstream::binary);
	fin.seekg(0, std::ios::end);
	size_t size = fin.tellg();
	file.resize(size, ' ');
	fin.seekg(0);
	fin.read(&file[0], size);
}

int main(int argc, char** argv)
{
	unsigned short port = 253;
	for (int i = 1; i < argc - 1; i++)
	{
		if (!strcmp(argv[i], "-p"))
		{
			port = atoi(argv[i + 1]);
			if (!port) port = 253;
		}
		else if(!strcmp(argv[i], "-l"))
		{
			if (freopen(argv[i + 1], "wt", stdout) == 0)
			{
				printf("Cannot open file for logging\n");
				continue;
			}
			setvbuf(stdout, NULL, _IONBF, 0);
		}
	}

	/*AES_KEYS aes;
	std::string key, source = "2561:15632", encrypted;
	aes.generate_keys(CryptoPP::AES::DEFAULT_KEYLENGTH);
	aes.get_key(key);
	printf("AES keys size: %d\n", key.size());
	aes.encrypt(source, encrypted);
	printf("AES encrypted message size: %d\n", encrypted.size());
	printf("AES encrypted message: %s\n", encrypted.c_str());
	aes.decrypt(encrypted, source);
	printf("AES decrypted message: %s\n", source.c_str());*/

	udp_controller controller(controller_listener);
	controller.start(port);

	std::string address, port_out, path, file;
	while (true)
	{
		std::cin >> address >> port_out >> path;
		read_file(path, file);
		printf("Message size: %d\n", file.size());
		controller.send(file, address.c_str(), atoi(port_out.c_str()));
	}

	controller.stop();
	fclose(stdout);
	return 0;
}