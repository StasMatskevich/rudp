#include "packet_separator.h"

void separate_window(const std::string& data, const int window_size, int& start, std::vector<std::string>& result)
{
	result.clear();

	if (data.size() - start < 1) return;

	unsigned short datagram_count = std::min(window_size / DATAGRAM_MAXIMUM_SIZE,
		(data.size() - start + SEPARATOR_DATA_SIZE - 1) / SEPARATOR_DATA_SIZE);

	for (unsigned short datagram_number = 0; datagram_number < datagram_count; datagram_number++)
	{
		unsigned int datagram_size = std::min(SEPARATOR_DATA_SIZE, data.size() - start);
		result.emplace_back(std::string(CONTROLLER_COMMAND_SIZE + SEPARATOR_COMMAND_SIZE + datagram_size, 0));
		result[datagram_number].resize(CONTROLLER_COMMAND_SIZE + SEPARATOR_COMMAND_SIZE + datagram_size);

		unsigned char type = MESSAGE_TYPE::DATA;
		memcpy(&result[datagram_number][0], &type, sizeof(unsigned char));
		type = 0;
		memcpy(&result[datagram_number][0] + sizeof(unsigned char), &type, sizeof(unsigned char));

		memcpy(&result[datagram_number][0] + CONTROLLER_COMMAND_SIZE, &datagram_number, sizeof(unsigned short));
		memcpy(&result[datagram_number][0] + sizeof(unsigned short) + CONTROLLER_COMMAND_SIZE, &datagram_count, sizeof(unsigned short));
		memcpy(&result[datagram_number][0] + SEPARATOR_COMMAND_SIZE + CONTROLLER_COMMAND_SIZE, &data[start], datagram_size);
		start += datagram_size;
	}
}
