#ifndef PACKET_SEPARATOR_H
#define PACKET_SEPARATOR_H

#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#include <vector>
#include "constants.h"
#include <algorithm>

void separate_window(const std::string& data, const int window_size, int& start, std::vector<std::string>& result);

#endif