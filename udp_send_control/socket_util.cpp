#include "socket_util.h"

bool wsa_init = false;

bool InitializeSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA WsaData;
	wsa_init = (WSAStartup(MAKEWORD(2, 2), &WsaData) == NO_ERROR);
#else
	wsa_init = true;
#endif
	return wsa_init;
}

bool isSocketsInitialized()
{
	return wsa_init;
}

void ShutdownSockets()
{
#if PLATFORM == PLATFORM_WINDOWS
	WSACleanup();
#endif
	wsa_init = false;
}