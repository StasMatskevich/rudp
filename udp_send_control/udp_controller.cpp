#include "udp_controller.h"

int udp_controller::send_window(udp_session& session, unsigned int window_id, unsigned int address, unsigned short port)
{
	for (int i = 0; i < session.get_window_size(); i++)
	{
		memcpy(&session.get_datagram(i)[0] + sizeof(unsigned char), &session.destination_session, sizeof(unsigned char));
		send_packet(session.get_datagram(i), address, port);
	}

	unsigned char type = MESSAGE_TYPE::WINDOW_SENDED;
	std::string message;
	message.resize(sizeof(unsigned char) * 2);
	memcpy(&message[0], &type, sizeof(unsigned char));
	memcpy(&message[0] + sizeof(unsigned char), &session.destination_session, sizeof(unsigned char));
	send_packet(message, address, port);

	session.set_state(SESSION_STATE::WAITING_FOR_WINDOW_CONFIRMATION);
	session.update_activity_time();

	return 0;
}

int udp_controller::send_packet(const std::string& packet, unsigned int address, unsigned short port)
{
	std::lock_guard<std::mutex> lock(send_mutex);
	return socket->send(&packet[0], packet.size(), address, port);
}

void udp_controller::socket_listener(sockaddr_in addr, socklen_t socklen, char* packet, int lenght)
{
	unsigned char type = 0;
	char dest_session_id = 0, local_session_id = 0;
	std::string message;
	memcpy(&type, packet, sizeof(unsigned char));
	unsigned int address = ntohl(addr.sin_addr.s_addr);
	unsigned short port = ntohs(addr.sin_port);
	unsigned short datagram_number = 0, datagram_count = 0, lost = 0;
	int window_size = 0, integrity_res = -1, window_id = -1;

	switch (type)
	{
		case(MESSAGE_TYPE::ASK_FOR_SESSION):
			local_session_id = get_session_id(sessions_in);
			if (local_session_id == -1)
			{
				message.resize(sizeof(unsigned char) * 2);

				type = MESSAGE_TYPE::SESSION_DENIED;
				memcpy(&dest_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));

				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &dest_session_id, sizeof(unsigned char));

				send_packet(message, address, port);
			}
			else
			{
				message.resize(sizeof(unsigned char) * 3 + sizeof(int));

				type = MESSAGE_TYPE::SESSION_ALLOWED;
				memcpy(&dest_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));

				unsigned int size;
				memcpy(&size, packet + sizeof(unsigned char) * 2, sizeof(unsigned int));

				sessions_in[local_session_id].set_state(SESSION_STATE::WAITING_FOR_DATA);
				sessions_in[local_session_id].setup(dest_session_id, size, address, port);
				sessions_in[local_session_id].update_activity_time();

				printf("[UDP controller] Incoming session started. Id: %d\n", local_session_id);

				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &dest_session_id, sizeof(unsigned char));
				memcpy(&message[0] + 2 * sizeof(unsigned char), &local_session_id, sizeof(unsigned char));
				memcpy(&message[0] + 3 * sizeof(unsigned char), &SESSION_BUFFER_SIZE, sizeof(int));

				send_packet(message, address, port);
			}
			break;
		case(MESSAGE_TYPE::SESSION_ALLOWED):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_out[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_SESSION) return;
			memcpy(&sessions_out[local_session_id].destination_session, packet + 2 * sizeof(unsigned char), sizeof(unsigned char));
			memcpy(&window_size, packet + 3 * sizeof(unsigned char), sizeof(int));

			sessions_out[local_session_id].set_state(SESSION_STATE::SENDING_DATA);
			sessions_out[local_session_id].prepare_data(window_size);
			sessions_out[local_session_id].update_activity_time();

			send_window(sessions_out[local_session_id], 0, address, port);
			break;
		case(MESSAGE_TYPE::SESSION_DENIED):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_out[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_SESSION) return;
			printf("[UDP controller] Session setup denied\n");
			break;
		case(MESSAGE_TYPE::DATA):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_in[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_DATA) return;
			memcpy(&datagram_number, packet + CONTROLLER_COMMAND_SIZE, sizeof(unsigned short));
			sessions_in[local_session_id].update_activity_time();
			
			memcpy(&datagram_count, packet + CONTROLLER_COMMAND_SIZE + sizeof(unsigned short), sizeof(unsigned short));
			sessions_in[local_session_id].set_packets_count(datagram_count);

			sessions_in[local_session_id].add_datagram(datagram_number, packet + CONTROLLER_COMMAND_SIZE + SEPARATOR_COMMAND_SIZE, lenght - CONTROLLER_COMMAND_SIZE - SEPARATOR_COMMAND_SIZE);
			break;
		case(MESSAGE_TYPE::WINDOW_SENDED):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_in[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_DATA) return;
			sessions_in[local_session_id].update_activity_time();

			integrity_res = sessions_in[local_session_id].check_for_integrity();
			if(integrity_res != -1)
			{
				type = MESSAGE_TYPE::ASK_FOR_LOST;
				lost = integrity_res;
				message.resize(sizeof(unsigned char) * 2 + sizeof(unsigned short));
				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &sessions_in[local_session_id].destination_session, sizeof(unsigned char));
				memcpy(&message[0] + CONTROLLER_COMMAND_SIZE, &lost, sizeof(unsigned short));
				send_packet(message, address, port);
			}
			else
			{
				type = MESSAGE_TYPE::SEND_NEXT_WINDOW;
				message.resize(sizeof(unsigned char) * 2);
				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &sessions_in[local_session_id].destination_session, sizeof(unsigned char));
				send_packet(message, address, port);
			}

			break;
		case(MESSAGE_TYPE::ASK_FOR_LOST):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_out[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_WINDOW_CONFIRMATION) return;
			sessions_out[local_session_id].update_activity_time();

			type = MESSAGE_TYPE::SEND_LOST;
			memcpy(&lost, packet + CONTROLLER_COMMAND_SIZE, sizeof(unsigned short));
			memcpy(&sessions_out[local_session_id].get_datagram(lost)[0], &type, sizeof(unsigned char));
			memcpy(&sessions_out[local_session_id].get_datagram(lost)[0] + sizeof(unsigned char), &sessions_out[local_session_id].destination_session, sizeof(unsigned char));
			send_packet(sessions_out[local_session_id].get_datagram(lost), address, port);
			break;
		case(MESSAGE_TYPE::SEND_NEXT_WINDOW):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_out[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_WINDOW_CONFIRMATION) return;

			window_id = sessions_out[local_session_id].get_next_window_id();
			if (window_id != -1)
			{
				sessions_out[local_session_id].set_state(SESSION_STATE::SENDING_DATA);
				sessions_out[local_session_id].update_activity_time();

				send_window(sessions_out[local_session_id], window_id, address, port);
			}
			else
			{
				type = MESSAGE_TYPE::DONE;
				sessions_out[local_session_id].set_state(SESSION_STATE::IDLE);
				message.resize(sizeof(unsigned char) * 2);
				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &sessions_out[local_session_id].destination_session, sizeof(unsigned char));
				send_packet(message, address, port);
				printf("[UDP controller] Outgoing session done. Id: %d\n", local_session_id);
			}
			break;
		case(MESSAGE_TYPE::SEND_LOST):
			if (sessions_in[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_DATA) return;
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			sessions_in[local_session_id].update_activity_time();

			integrity_res = sessions_in[local_session_id].check_for_integrity();
			if (integrity_res != -1)
			{
				sessions_in[local_session_id].add_datagram(integrity_res, packet + CONTROLLER_COMMAND_SIZE + SEPARATOR_COMMAND_SIZE, lenght - CONTROLLER_COMMAND_SIZE - SEPARATOR_COMMAND_SIZE);
			}

			integrity_res = sessions_in[local_session_id].check_for_integrity();
			if (integrity_res != -1)
			{
				type = MESSAGE_TYPE::ASK_FOR_LOST;
				lost = integrity_res;
				message.resize(sizeof(unsigned char) * 2 + sizeof(unsigned short));
				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &sessions_in[local_session_id].destination_session, sizeof(unsigned char));
				memcpy(&message[0] + CONTROLLER_COMMAND_SIZE, &lost, sizeof(unsigned short));
				send_packet(message, address, port);
			}
			else
			{
				type = MESSAGE_TYPE::SEND_NEXT_WINDOW;
				message.resize(sizeof(unsigned char) * 2);
				memcpy(&message[0], &type, sizeof(unsigned char));
				memcpy(&message[0] + sizeof(unsigned char), &sessions_in[local_session_id].destination_session, sizeof(unsigned char));
				send_packet(message, address, port);
			}
			break;
		case(MESSAGE_TYPE::DONE):
			memcpy(&local_session_id, packet + sizeof(unsigned char), sizeof(unsigned char));
			if (sessions_in[local_session_id].get_state() != SESSION_STATE::WAITING_FOR_DATA) return;
			on_got_message(sessions_in[local_session_id].get_string(), address, port);
			printf("[UDP controller] Incoming session done. Id: %d\n", local_session_id);
			sessions_in[local_session_id].set_state(SESSION_STATE::IDLE);
			break;
	}
	return;
}

bool udp_controller::socket_error(int error)
{
	if (error == 10038) return true;
	if (error == 10004) return true;
	return false;
}

int udp_controller::get_session_id(std::vector<udp_session>& sessions)
{
	for (int i = 0; i < sessions.size(); i++)
	{
		if (sessions[i].get_state() == SESSION_STATE::IDLE)
		{
			sessions[i].clear();
			return i;
		}
	}
	return -1;
}

udp_controller::udp_controller(std::function<void(std::string&, unsigned int, unsigned short)> message_callback)
{
	if(!isSocketsInitialized()) InitializeSockets();
	started = false;
	sessions_in.resize(MAXIMUM_SESSIONS, udp_session());
	sessions_out.resize(MAXIMUM_SESSIONS, udp_session());
	on_got_message = message_callback;
	socket = 0;

	check_thread = std::thread(
	[&]()
	{
		SESSION_STATE state;
		while (true)
		{
			if (started)
			{
				for (int i = 0; i < sessions_in.size(); i++)
				{
					if (sessions_in[i].get_state() != SESSION_STATE::IDLE)
					{
						time_t cur_time;
						time(&cur_time);
						double inactive_time = difftime(cur_time, sessions_in[i].get_activity_time());
						if (inactive_time > INCOMING_SESSION_TIMEOUT_IN_SECONDS)
						{
							sessions_in[i].set_state(SESSION_STATE::IDLE);
							printf("[UDT controller] Incoming session with id(%d) timed out. Session closed\n", i);
						}
					}
				}

				for (int i = 0; i < sessions_out.size(); i++)
				{
					state = sessions_out[i].get_state();
					if (state != SESSION_STATE::IDLE && state != SESSION_STATE::SENDING_DATA)
					{
						time_t cur_time;
						time(&cur_time);
						double inactive_time = difftime(cur_time, sessions_out[i].get_activity_time());
						if (inactive_time > OUTGOING_SESSION_TIMEOUT_IN_SECONDS)
						{
							if(sessions_out[i].should_retry())
							{
								if (state == SESSION_STATE::WAITING_FOR_SESSION)
								{
									printf("[UDT controller] Outgoing session with id(%d) timed out, retrying\n", i);

									std::string message;
									unsigned char type = MESSAGE_TYPE::ASK_FOR_SESSION;
									unsigned int size = sessions_out[i].get_string().size();
									message.resize(sizeof(unsigned char) * 2 + sizeof(unsigned int));

									memcpy(&message[0], &type, sizeof(unsigned char));
									memcpy(&message[0] + sizeof(unsigned char), &i, sizeof(unsigned char));
									memcpy(&message[0] + sizeof(unsigned char) * 2, &size, sizeof(unsigned int));

									send_packet(message, sessions_out[i].destination_address, sessions_out[i].destination_port);

									sessions_out[i].update_activity_time();
								}
								else if (state == SESSION_STATE::WAITING_FOR_WINDOW_CONFIRMATION)
								{
									printf("[UDT controller] Outgoing session with id(%d) timed out, retrying\n", i);

									std::string message;
									unsigned char type = MESSAGE_TYPE::WINDOW_SENDED;
									message.resize(sizeof(unsigned char) * 2);
									memcpy(&message[0], &type, sizeof(unsigned char));
									memcpy(&message[0] + sizeof(unsigned char), &sessions_out[i].destination_session, sizeof(unsigned char));
									send_packet(message, sessions_out[i].destination_address, sessions_out[i].destination_port);

									sessions_out[i].update_activity_time();
								}
								else
								{
									sessions_out[i].set_state(SESSION_STATE::IDLE);
									printf("[UDT controller] Outgoing session with id(%d) timed out and cannot be retried. Session closed\n", i);
								}
							}
							else
							{
								sessions_out[i].set_state(SESSION_STATE::IDLE);
								printf("[UDT controller] Outgoing session with id(%d) timed out, too much retries. Session closed\n", i);
							}
						}
					}
				}
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	});
	check_thread.detach();
}

udp_controller::~udp_controller()
{
	stop();
}

int udp_controller::start(unsigned short port)
{
	if (started)
	{
		printf("[UDP controller] Controller already started\n");
		return -1;
	}

	socket = new udp_socket();

	if (socket == 0)
	{
		printf("[UDP controller] Socket pointer is null\n");
		return -2;
	}

	int err = socket->open(port);
	if (err)
	{
		printf("[UDP controller] Socket open error\n");
		return -3;
	}

	err = socket->set_buffers_size(BUFFER_SIZE);
	if (err)
	{
		printf("[UDP controller] Socket buffers set error\n");
		return -4;
	}

	err = socket->listen([&](sockaddr_in addr, socklen_t socklen, char* packet, int lenght) {socket_listener(addr, socklen, packet, lenght); }, 
						 [&](int error) -> bool{return socket_error(error); });
	if (err)
	{
		printf("[UDP controller] Socket listener errror\n");
		return -5;
	}

	started = true;
	printf("[UDP controller] Controller started on %d port\n", port);

	return 0;
}

int udp_controller::stop()
{
	if (socket != 0)
	{
		socket->close();
		free(socket);
	}

	started = false;
	return 0;
}

int udp_controller::send(const std::string& data, unsigned int address, unsigned short port)
{
	if (!started)
	{
		printf("[UDP controller] Tried to send using unstarted controller\n");
		return -4;
	}

	if (data.empty())
	{
		printf("[UDP controller] Tried to send empty data\n");
		return -3;
	}

	if (data.size() > ~((unsigned int)0))
	{
		printf("[UDP controller] Tried to send too big data. Maximum size: %d\n", ~((unsigned int)0));
		return -2;
	}

	char session_id = get_session_id(sessions_out);
	if (session_id == -1)
	{
		//should add some queue for this case?
		printf("[UDP controller] Out sessions limit reached\n");
		return -1;
	}

	sessions_out[session_id].set_state(SESSION_STATE::WAITING_FOR_SESSION);
	sessions_out[session_id].setup(data, address, port);
	sessions_out[session_id].update_activity_time();

	std::string message;
	unsigned char type = MESSAGE_TYPE::ASK_FOR_SESSION;
	message.resize(sizeof(unsigned char) * 2 + sizeof(unsigned int));

	unsigned int size = data.size();
	memcpy(&message[0], &type, sizeof(unsigned char));
	memcpy(&message[0] + sizeof(unsigned char), &session_id, sizeof(unsigned char));
	memcpy(&message[0] + sizeof(unsigned char) * 2, &size, sizeof(unsigned int));

	printf("[UDP controller] Outgoing session started. Id: %d\n", session_id);

	return send_packet(message, address, port);
}

int udp_controller::send(const std::string& data, const char * address, unsigned short port)
{
	unsigned int a, b, c, d;
	if (sscanf_s(address, "%d.%d.%d.%d", &a, &b, &c, &d) < 4)
	{
		printf("[UDP controller] Wrong address format. Must be: $d.$d.$d.$d. Example: 127.0.0.1\n");
		return -2;
	}
	unsigned int destination_address = (a << 24) | (b << 16) | (c << 8) | d;
	return send(data, destination_address, port);
}
