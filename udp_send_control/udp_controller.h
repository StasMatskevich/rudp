#ifndef UDP_CONTROLLER_H
#define UDP_CONTROLLER_H

#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#include "udp_socket.h"
#include "udp_session.h"

class udp_controller
{
public:
	udp_controller(std::function<void(std::string&, unsigned int, unsigned short)> message_callback);
	~udp_controller();

	int start(unsigned short port);
	int stop();

	int send(const std::string& data, const char* address, unsigned short port);
	int send(const std::string& data, unsigned int address, unsigned short port);
private:
	int send_window(udp_session& session, unsigned int window_id, unsigned int address, unsigned short port);
	int send_packet(const std::string& packet, unsigned int address, unsigned short port);
	void socket_listener(sockaddr_in addr, socklen_t socklen, char* packet, int lenght);
	bool socket_error(int error);
	int get_session_id(std::vector<udp_session>& sessions);

	std::vector<udp_session> sessions_in, sessions_out;
	udp_socket* socket;
	bool started;
	std::mutex send_mutex;
	std::function<void(std::string, unsigned int, unsigned short)> on_got_message;
	std::thread check_thread;
};

#endif
