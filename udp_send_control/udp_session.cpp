#include "udp_session.h"

udp_session::udp_session()
{
	set_state(SESSION_STATE::IDLE);
}

udp_session::udp_session(const udp_session & other)
{
	string_data = other.string_data;
	destination_address = other.destination_address;
	destination_port = other.destination_port;
	set_state(SESSION_STATE::IDLE);
}

udp_session::~udp_session()
{
	clear();
}

void udp_session::setup(const std::string& raw_data, unsigned int address, unsigned short port)
{
	string_data = raw_data;
	destination_address = address;
	destination_port = port;
	current_window_id = 0;
	checked_packets = 0;
	packets_in_window = -1;
	window_start = 0;
	window_size = 0;
}

void udp_session::setup(unsigned char session, unsigned int size, unsigned int address, unsigned short port)
{
	destination_address = address;
	destination_port = port;
	destination_session = session;
	current_window_id = 0;
	checked_packets = 0;
	packets_in_window = -1;
	window_start = 0;
	window_size = 0;
	string_data.reserve(size);
	std::lock_guard<std::mutex> lock(state_mutex);
	retries = 0;
}

int udp_session::prepare_data(int current_window_size)
{
	window_size = current_window_size;
	separate_window(string_data, window_size, window_start, current_window);
	return 0;
}

std::string& udp_session::get_datagram(int datagram)
{
	return current_window[datagram];
}

std::string& udp_session::get_string()
{
	return string_data;
}

void udp_session::add_datagram(int datagram, char *packet, int leght)
{
	current_window[datagram].resize(leght);
	memcpy(&current_window[datagram][0], packet, leght);
}

void udp_session::set_packets_count(int count)
{
	if (packets_in_window != -1) return;
	window_start += count * SEPARATOR_DATA_SIZE;
	packets_in_window = count;
	current_window.resize(count);
}

int udp_session::check_for_integrity()
{
	for (checked_packets; checked_packets < current_window.size(); checked_packets++)
	{
		if (current_window[checked_packets].empty())
			return checked_packets;
		string_data += current_window[checked_packets];
	}

	if (current_window.size() < packets_in_window)
		return checked_packets;

	current_window_id++;
	current_window.clear();
	checked_packets = 0;
	packets_in_window = -1;

	return -1;
}

bool udp_session::should_retry()
{
	std::lock_guard<std::mutex> lock(state_mutex);
	if (retries < SESSION_RETRIES_AFTER_TIMEOUT)
	{
		retries++;
		return true;
	}
	return false;
}

void udp_session::set_state(SESSION_STATE new_state)
{
	std::lock_guard<std::mutex> lock(state_mutex);
	state = new_state;
	retries = 0;
}

SESSION_STATE udp_session::get_state()
{
	std::lock_guard<std::mutex> lock(state_mutex);
	return state;
}

void udp_session::update_activity_time()
{
	std::lock_guard<std::mutex> lock(time_mutex);
	time(&activity_time);
}

time_t udp_session::get_activity_time()
{
	std::lock_guard<std::mutex> lock(time_mutex);
	return activity_time;
}

int udp_session::get_next_window_id()
{
	if(window_start >= string_data.size()) return -1;
	current_window_id++;
	separate_window(string_data, window_size, window_start, current_window);
	return current_window_id;
}

int udp_session::get_window_size()
{
	return current_window.size();
}

void udp_session::clear()
{
	string_data.clear();
	current_window.clear();
	update_activity_time();
}
