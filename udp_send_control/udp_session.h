#ifndef UDP_SESSION_H
#define UDP_SESSION_H

#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#include "packet_separator.h"
#include <mutex>

class udp_session
{
public:
	udp_session();
	udp_session(const udp_session& other);
	~udp_session();

	void setup(const std::string& string_data, unsigned int address, unsigned short port);
	void setup(unsigned char session, unsigned int size, unsigned int address, unsigned short port);
	int prepare_data(int current_window_size);
	std::string& get_datagram(int datagram);
	std::string& get_string();
	void add_datagram(int datagram, char * packet, int leght);
	void set_packets_count(int count);
	int check_for_integrity();
	bool should_retry();

	void set_state(SESSION_STATE new_state);
	SESSION_STATE get_state();

	void update_activity_time();
	time_t get_activity_time();
	int get_next_window_id();
	int get_window_size();

	void clear();

	unsigned int destination_address;
	unsigned short destination_port;
	unsigned char destination_session;
private:
	int checked_packets, current_window_id, packets_in_window, window_start, window_size, retries;
	std::string string_data;
	std::vector<std::string> current_window;
	SESSION_STATE state;
	std::mutex state_mutex, time_mutex;
	time_t activity_time;
};

#endif