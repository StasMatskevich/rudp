#include "udp_socket.h"

udp_socket::udp_socket()
{
	created = false;
	opened = false;
	listening = false;

	try
	{
		if (!isSocketsInitialized()) throw std::exception("Sockets not initialized");
		handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		created = (handle != INVALID_SOCKET);
	}
	catch (std::exception& e)
	{
		printf("[UDP socket] Socket constructor error: %s\n", e.what());
	}
}

udp_socket::~udp_socket()
{
	close();
}

int udp_socket::open(unsigned short port)
{
	if (!created)
	{
		printf("[UDP socket] Trying open not created socket\n");
		return -1;
	}

	if (opened)
	{
		printf("[UDP socket] Socket already opened\n");
		return -2;
	}

	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons((unsigned short)port);

	if (bind(handle, (const sockaddr*)&address, sizeof(sockaddr_in)) == SOCKET_ERROR)
	{
		printf("[UDP socket] Bind error: %d\n", WSAGetLastError());
		return -3;
	}

	opened = true;
	return 0;
}

int udp_socket::set_buffers_size(int size)
{
	return setsockopt(handle, SOL_SOCKET, SO_RCVBUF, (char*)&size, sizeof(int));
	return setsockopt(handle, SOL_SOCKET, SO_SNDBUF, (char*)&size, sizeof(int));
}

void udp_socket::close()
{
	#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
	close(handle);
	#elif PLATFORM == PLATFORM_WINDOWS
	closesocket(handle);
	#endif
	listening = false;
	opened = false;
}

bool udp_socket::is_open()
{
	return opened;
}

bool udp_socket::is_listening()
{
	return listening;
}

int udp_socket::listen(std::function<void(sockaddr_in, socklen_t, char*, int)> on_accept, std::function<bool(int)> on_error)
{
	if (!opened)
	{
		printf("[UDP socket] Trying listening on not opened socket\n");
		return -1;
	}
	if (listening)
	{
		printf("[UDP socket] Socket already listening\n");
		return -2;
	}
	listening = true;
	listen_thread = std::thread([on_accept, on_error](int handle)
	{
		while (true)
		{
			char packet_data[DATAGRAM_MAXIMUM_SIZE + 1] = { '\0' };
			unsigned int maximum_packet_size = sizeof(packet_data);

			sockaddr_in from;
			socklen_t fromLength = sizeof(from);

			int received_bytes = recvfrom(handle, (char*)packet_data, maximum_packet_size, 0, (sockaddr*)&from, &fromLength);

			if (received_bytes == SOCKET_ERROR)
			{
				if (on_error(WSAGetLastError())) break;
				continue;
			}

			if (received_bytes == 0)
				continue;

			//printf("[UDP socket] Got packet %d\n", received_bytes);
			on_accept(from, fromLength, packet_data, received_bytes);
		}
	}, handle);
	listen_thread.detach();
	return 0;
}

int udp_socket::send(const char* packet_data, unsigned int packet_size, const char* address, unsigned short port)
{
	unsigned int a, b, c, d;
	if (sscanf_s(address, "%d.%d.%d.%d", &a, &b, &c, &d) < 4)
	{
		printf("[UDP socket] Wrong address format. Must be: $d.$d.$d.$d. Example: 127.0.0.1\n");
		return -2;
	}
	unsigned int destination_address = (a << 24) | (b << 16) | (c << 8) | d;
	return send(packet_data, packet_size, destination_address, port);
}

int udp_socket::send(const char* packet_data, unsigned int packet_size, unsigned int address, unsigned short port)
{
	if (packet_size > DATAGRAM_MAXIMUM_SIZE)
	{
		printf("[UDP socket] Can't send more than %d bytes in one packet. Input size: %d\n", DATAGRAM_MAXIMUM_SIZE, packet_size);
		return -1;
	}

	unsigned short destination_port = port;

	sockaddr_in sendAddress;
	sendAddress.sin_family = AF_INET;
	sendAddress.sin_addr.s_addr = htonl(address);
	sendAddress.sin_port = htons(destination_port);

	int sent_bytes = sendto(handle, (const char*)packet_data, packet_size, 0, (sockaddr*)&sendAddress, sizeof(sockaddr_in));

	if (sent_bytes != packet_size)
	{
		return sent_bytes;
	}

	//printf("[UDP socket] Bytes sent %d\n", sent_bytes);

	return 0;
}


