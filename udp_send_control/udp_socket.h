#ifndef UDP_SOCKET_H
#define UDP_SOCKET_H

#ifdef _MSC_VER
#pragma once
#endif  // _MSC_VER

#include "socket_util.h"
#include "constants.h"
#include <thread>

class udp_socket
{
public:

	udp_socket();
	~udp_socket();
	int open(unsigned short port);
	int set_buffers_size(int size);
	void close();
	bool is_open();
	bool is_listening();
	int listen(std::function<void(sockaddr_in, socklen_t, char*, int)> on_accept, std::function<bool(int)> on_error);
	int send(const char* packet_data, unsigned int packet_size, const char* address, unsigned short port);
	int send(const char* packet_data, unsigned int packet_size, unsigned int address, unsigned short port);

private:
	bool created, opened, listening;
	int handle;
	std::thread listen_thread;
};

#endif